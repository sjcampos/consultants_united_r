ID_AGENCY,IDENTIFICATION,NAME_AGENCY,PROVINCE_NAME,CANTON_NAME,DISTRICT_NAME
1,1,CentralCQ,Alajuela,San Carlos,Ciudad Quesada
2,1,CentralAla,Alajuela,Alajuela,Alajuela
3,2,CentralHeredia,Heredia,Flores,San Joaquin de Flores
4,2,DistBarva,Heredia,Barva,Barva
5,2,DistIsidro,Heredia,San Isidro,Concepcion
6,3,CentralCartago,Cartago,Cartago,Corralillo
7,3,DistParaiso,Cartago,Paraiso,Paraiso