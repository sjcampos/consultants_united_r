# Consultants_united_Project3

## Introducción

• Como consultor de la empresa, Grupo Innova S.A, usted fue contratado para realizar el análisis la información utilizando técnicas de minería de datos con machine learning
• Entre las estratégicas basadas en datos en datos,se pretende predecir las ventas futuras y si los clientes volverán a comprar y segmentar a los clientes para redirigir campañas
de marketing.

## Herramientas utilizadas

1. Rstudio

2. Rtools

3. SQL Server

4. SQL Server Managment Studio


## Estructura y rutas de repositorio

Seguidamente se listarán los links para accesar a diferentes archivos dentro de la carpeta del repositorio.

 - [Link del repositorio](https://gitlab.com/sjcampos/consultants_united_r.git)
 - [Documentación](/Enunciado1_Documentos/Proyecto_3.pdf)
 - [Etapa EDA](/Scripts/EDA.pdf)
 - [Etapa Ingerir](/Scripts/Ingerir.pdf)
 - [Etapa Modelos](/Scripts/modelos.pdf)